--Level Location
INSERT INTO public.m_location_level(name, abbreviation, created_by, created_on, is_delete)
VALUES
    ('Provinsi', 'Prov', 1, now(), false),
    ('Kabupaten', 'Kab', 1, now(), false),
    ('Kota', 'Kota', 1, now(), false),
    ('Kecamatan', 'Kec', 1, now(), false);

--Location
INSERT INTO public.m_location(name, parent_id, location_level_id, created_by, created_on, is_delete)
VALUES
    ('Malang', null, 3, 1, now(), false),
    ('Lowakwaru', 1, 4, 1, now(), false),
    ('Klojen', 1, 4, 1, now(), false),
    ('Blimbing', 1, 4, 1, now(), false),
    ('Batu', null, 3, 1, now(), false),
    ('Bumiaji', 5, 4, 1, now(), false),
    ('Batu', 5, 4, 1, now(), false);


--Biodata
INSERT INTO public.m_biodata(fullname, mobile_phone, created_by, created_on, is_delete)
VALUES
    ('Abdullah Syafiq Ammar', '081253723972', 1, now(),false),
    ('Blasius Zuriel Zeke Manurung', '085248931023', 1, now(),false),
    ('Ilham Ramadhan Putra', '087253084016', 1, now(),false),
    ('Moch. Septian Firmansyah', '082842946738', 1, now(),false),
    ('Faiq Maulana', '087435982909', 1, now(),false);

--Doctor
INSERT INTO public.m_doctor(biodata_id, str, created_by, created_on, is_delete)
VALUES
      (1, null, 1, now(), false),
      (2, null, 1, now(), false),
      (3, null, 1, now(), false),
      (4, null, 1, now(), false),
      (5, null, 1, now(), false);

--Doctor Treatment
INSERT INTO public.t_doctor_treatment(doctor_id, name, created_by, created_on, is_delete)
VALUES
      (1, 'Konsultasi Kesehatan Umum', 1, now(), false),
      (1, 'Medical Check up', 1, now(), false),
      (1, 'Perawatan Luka', 1, now(), false),

      (2, 'Konsultasi Kesehatan Umum', 1, now(), false),
      (2, 'Medical Check up', 1, now(), false),
      (2, 'Perawatan Luka', 1, now(), false),

      (3, 'Konsultasi Kesehatan Umum', 1, now(), false),
      (3, 'Medical Check up', 1, now(), false),
      (3, 'Perawatan Luka', 1, now(), false),

      (4, 'Konsultasi Kesehatan Umum', 1, now(), false),
      (4, 'Medical Check up', 1, now(), false),
      (4, 'Perawatan Luka', 1, now(), false),

      (5, 'Konsultasi Kesehatan Umum', 1, now(), false),
      (5, 'Medical Check up', 1, now(), false),
      (5, 'Perawatan Luka', 1, now(), false);

--Medical Facility Category
INSERT INTO public.m_medical_facility_category(name, created_by, created_on, is_delete)
VALUES
    ('RSUD', 1, now(), false),
    ('Puskesmas', 1, now(), false),
    ('Klinik', 1, now(), false);

--Medical Facility
INSERT INTO public.m_medical_facility(name, medical_facility_category_id, location_id, full_address , email, phone_code, phone, fax, created_by, created_on, is_delete)
VALUES
      ('RSUD. Dr. Saiful Anwar Malang', 1, 2, 'Jl. Jaksa Agung Suprapto No.2, Klojen, Kec. Klojen, Kota Malang, Jawa Timur 65112', null, null, null, null, 1, now(), false),
      ('RS Universitas Brawijaya', 1, 3, 'Jl. Soekarno - Hatta, Lowokwaru, Kec. Lowokwaru, Kota Malang, Jawa Timur 65141', null, null, null, null, 1, now(), false),
      ('RS Persada Hospital', 1, 4, 'Jl. Raden Panji Suroso KAV.II-IV, Purwodadi, Kec. Blimbing, Kota Malang, Jawa Timur 65126', null, null, null, null, 1, now(), false),
      ('RS Punten', 1, 6, 'Jl. Kenanga No.300, Bulukerto, Kec. Bumiaji, Kota Batu, Jawa Timur 65338', null, null, null, null, 1, now(), false),
      ('RSUD Karsa Husada', 1, 7, 'Jl. Ahmad Yani No.11, Ngaglik, Kec. Batu, Kota Batu, Jawa Timur 65311', null, null, null, null, 1, now(), false);

--Doctor Office
INSERT INTO public.t_doctor_office(doctor_id, medical_facility_id, specialization, start_date, end_date, created_by, created_on, is_delete)
VALUES
      (1, 1, 'Dokter Umum', '2017-10-20', now(), 1, now(), false),
      (1, 2, 'Dokter Umum', '2017-10-20', now(), 1, now(), false),
      (1, 5, 'Dokter Umum', '2017-10-20', now(), 1, now(), false),
      (2, 2, 'Dokter Kandungan', '2016-05-10', now(), 1, now(), false),
      (2, 3, 'Dokter Kandungan', '2016-05-10', now(), 1, now(), false),
      (3, 3, 'Dokter Mata', '2017-01-10', now(), 1, now(), false),
      (3, 1, 'Dokter Mata', '2017-01-10', now(), 1, now(), false),
      (4, 4, 'Dokter THT', '2019-10-10', now(), 1, now(), false),
      (4, 5, 'Dokter THT', '2019-10-10', now(), 1, now(), false),
      (4, 4, 'Dokter THT', '2019-10-10', now(), 1, now(), false),
      (5, 5, 'Dokter Umum', '2018-06-10', now(), 1, now(), false);

