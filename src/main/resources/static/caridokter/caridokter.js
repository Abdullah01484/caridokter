$("#addBtnCari").click(function(){
	$.ajax({
		url: "/caridokter/caridokter",
        type: "GET",
        contentType: "html",
        success:function(data){
			$(".modal-title").text("Cari Dokter");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
})

$("#addBtnAturUlang").click(function(){
    var originalModalContent = $(".modal .modal-content").html();

    $("#addBtnAturUlang").click(function(){
        $(".modal  .modal-content").html(originalModalContent);
        // Tampilkan modal
        $(".modal").modal("show");
    });
});

function opsilocation(){
    $("#lokasi").html(``);
    $("#lokasi").empty();
    $("#lokasi").append('<option value="">Pilih</option>');
    $.ajax({
        url : "/api/getalllocation",
        type : "GET",
        contentType : "application/json",
        success : function(data){
            for(i = 0; i<data.length; i++){
                $("#lokasi").append(
                `<option value="${data[i].id}">${data[i].name}</option>`
                )
            }
        }
    });
}

function opsiSpecialization(){
    $("#spesialisasi").html(``);
    $("#spesialisasi").empty();
    $("#spesialisasi").append('<option value="">Pilih</option>');
    $.ajax({
        url : "/api/getallspesialisasidokter",
        type : "GET",
        contentType : "application/json",
        success : function(data){
            for(i = 0; i<data.length; i++){
                $("#spesialisasi").append(
                `<option value="${data[i].id}">${data[i].name}</option>`
                )
            }
        }
    });
}

function opsiTreatment(){
    $("#tindakanMedis").html(``);
    $("#tindakanMedis").empty();
    $("#tindakanMedis").append('<option value="">Pilih</option>');
    $.ajax({
        url : "/api/getalldoktertreatment",
        type : "GET",
        contentType : "application/json",
        success : function(data){
            for(i = 0; i<data.length; i++){
                $("#tindakanMedis").append(
                `<option value="${data[i].id}">${data[i].name}</option>`
                )
            }
        }
    });
}

$(document).ready(function(){
    opsilocation();
    opsiSpecialization();
    opsiTreatment();
})

function dashboardcaridokter(){
	$.ajax({
		url: "/caridokter/dashboardcaridokter",
        type: "GET",
        contentType: "html",
        success:function(data){
            $.ajax({
                url: "/caridokter/dashboardcaridokter",
                type: "GET",
                contentType: "html",
                success:function(data){

            }
		}
	});
}
