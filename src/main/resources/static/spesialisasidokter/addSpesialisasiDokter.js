$("#addCancelBtn").click(function(){
	$(".modal").modal("hide")
})

$("#addCreateBtn").click(function(){
	var nama = $("#namaInput").val().trim();

	if(nama == ""){
		$("#errNama").text("Name tidak boleh kosong!");
		return;
	} else {
		$("#errNama").text("");
	}

	addSpesialisasiDokterName(function(spesialisname) {
	    var isNameExist = spesialisname.some(function(item) {
	        return item.name.toLowerCase() === nama.toLowerCase();
        });

        if (isNameExist) {
            $("#errNama").text("Nama sudah ada!");
            return;
        } else {
            $("#errNama").text("");
        }

	    var obj = {};
	    obj.name = nama;

	    var myJson = JSON.stringify(obj);

	    $.ajax({
		    url : "/api/addspesialisasidokter",
		    type : "POST",
		    contentType : "application/json",
		    data : myJson,
		    success: function(data){
				$(".modal").modal("hide")
				location.reload();
		    },
		    error: function(){
			    alert("Terjadi kesalahan")
		    }
		});
	});
})

function addSpesialisasiDokterName(callback) {
    $.ajax({
        url: "/api/getallspesialisasidokter",
        type: "GET",
        contentType: "application/json",
        success: function(spesialisname) {
            if (callback && typeof callback === "function") {
                callback(spesialisname);
            }
        },
        error: function(error) {
            if (callback && typeof callback === "function") {
                callback(error);
            }
        }
    });
}