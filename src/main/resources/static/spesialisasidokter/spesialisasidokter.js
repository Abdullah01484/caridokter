function getAllSpesialisasiDokter(){
	$("#spesialisasidokterTable").html(
		`<thead>
			<tr>
				<th width = '60%'>NAMA</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody id="spesialisasidokterTBody"></tbody>
		`
	);

//	$.ajax({
//		url : "/api/getallspesialisasidokter",
//		type : "GET",
//		contentType : "application/json",
//		success: function(data){
//			for(i = 0; i<data.length; i++){
//				$("#spesialisasidokterTBody").append(
//					`
//					<tr>
//						<td>${data[i].name}</td>
//                        <td>
//                        	<button value="${data[i].id}" onClick="editSpesialisasiDokter(this.value)" class="btn btn-warning">
//                        	    <i class="bi-pencil-square"> Edit</i>
//                        	</button>
//                        	<button value="${data[i].id}" onClick="deleteSpesialisasiDokter(this.value)" class="btn btn-danger">
//                        	    <i class="bi-trash"> Delete</i>
//                        	</button>
//                        </td>
//					</tr>
//					`
//				)
//			}
//		}
//	});
}

$("#addBtn").click(function(){
	$.ajax({
		url: "/spesialisasidokter/addspesialisasidokter",
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Tambah Spesialisasi Dokter");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
})

function editSpesialisasiDokter(id){
	$.ajax({
		url: "/spesialisasidokter/editspesialisasidokter/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Edit Spesialisasi Dokter");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

function deleteSpesialisasiDokter(id){
	$.ajax({
		url: "/spesialisasidokter/deletespesialisasidokter/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Delete Spesialisasi Dokter");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

function SpesialisasiDokterList(currentPage, length) {
    $.ajax({
		url : '/api/mappedspesialisasidokter?page=' + currentPage + '&size=' + length,
		type : 'GET',
		contentType : 'application/json',
		success : function(data) {

			let table = "<table class='table table-bordered mt-3'>"

			table += "<tr> <th width='60%' >NAMA</th> <th>Action</th> </tr>"
			for (let i = 0; i < data.spesialisasidokter.length; i++) {
				table += "<tr>";
				table += "<td>" + data.spesialisasidokter[i].name + "</td>";
				table += "<td><button class='btn btn-primary btn-sm' value='" + data.spesialisasidokter[i].id + "' onclick=editSpesialisasiDokter(this.value)><i class='bi-pencil-square'>  Edit</i></button> <button class='btn btn-danger btn-sm' value='" + data.spesialisasidokter[i].id + "' onclick=deleteSpesialisasiDokter(this.value)><i class='bi-trash'></i> Delete</button></td>";
				table += "</tr>";
			}
				table += "</table>";
				table += "<br>"
				table += '<nav aria-label="Page navigation">';
				table += '<ul class="pagination">'
				table += '<li class="page-item"><a class="page-link" onclick="SpesialisasiDokterList(' + (data.currentPage - 1) + ',' + length + ')">Previous</a></li>'
				let index = 1;
				for (let i = 0; i < data.totalPages; i++) {
					table += '<li class="page-item"><a id="pageslink" class="page-link" onclick="SpesialisasiDokterList(' + i + ',' + length + ')">' + index + '</a></li>'
					index++;
				}
			table += '<li class="page-item"><a class="page-link" onclick="SpesialisasiDokterList(' + (data.currentPage + 1) + ',' + length + ')">Next</a></li>'
			table += '</ul>'
			table += '</nav>';
			$('#spesialisasidokterList').html(table);
		}

	});
}

function SearchSpesialisasiDokter(request) {
//console.log(request)

    if (request.length > 0)
	{
	    $.ajax({
			url: '/api/searchspesialisasidokter/' + request,
			type: 'GET',
			contentType: 'application/json',
			success: function (result) {
			    let table = "<table class='table table-bordered mt-3'>";
				table += "<tr> <th width='60%' >NAMA</th> <th>Action</th> </tr>"
			    if (result.length > 0)
			    {
				    for (let i = 0; i < result.length; i++) {
					    table += "<tr>";
                        table += "<td>" + result[i].name + "</td>";
                        table += "<td><button class='btn btn-primary btn-sm' value='" + result[i].id + "' onclick=editSpesialisasiDokter(this.value)><i class='bi-pencil-square'>  Edit</i></button> <button class='btn btn-danger btn-sm' value='" +  result[i].id + "' onclick=deleteSpesialisasiDokter(this.value)><i class='bi-trash'></i> Delete</button></td>";
						table += "</tr>";
					}
				} else {
				    table += "<tr>";
				    table += "<td colspan='2' class='text-center'>No data</td>";
				    table += "</tr>";
		        }
				table += "</table>";
				$('#spesialisasidokterList').html(table);
			}
		});
	} else {
        SpesialisasiDokterList(0,5);
	}
}

$(document).ready(function(){
    SpesialisasiDokterList(0,5);
//    getAllSpesialisasiDokter();
})