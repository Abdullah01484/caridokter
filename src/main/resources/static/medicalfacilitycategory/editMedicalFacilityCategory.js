$(document).ready(function() {
	GetMedicalFacilityCategoryById();

})

var createdOn;
var createdBy;

function GetMedicalFacilityCategoryById() {
	var id = $("#editMedicalFacilityCategoryId").val();
	$.ajax({
		url: "/api/getbyidmedicalfacilitycategory/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
		    createdBy = data.created_By;
            createdOn = data.created_On;
			$("#namaInput").val(data.name);
		}
	});
}

$("#editBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#editBtnCreate").click(function() {
    var id = $("#editMedicalFacilityCategoryId").val();
	var nama = $("#namaInput").val();

	if (nama == "") {
		$("#errNama").text("Name tidak boleh kosong!");
		return;
	} else {
		$("#errNama").text("");
	}

	var obj = {};
	obj.created_By = createdBy;
    obj.created_On = createdOn;
	obj.id = id;
	obj.name = nama;

	var myJson = JSON.stringify(obj);

	$.ajax({
		url: "/api/editmedicalfacilitycategory/" + id,
		type: "PUT",
		contentType: "application/json",
		data: myJson,
		success: function(data) {
				$(".modal").modal("hide")
				location.reload();
		},
		error: function() {
			alert("Terjadi kesalahan")
		}
	});
})
