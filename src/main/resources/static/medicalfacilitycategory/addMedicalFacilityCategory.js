$("#addCancelBtn").click(function(){
	$(".modal").modal("hide")
})

$("#addCreateBtn").click(function(){
	var nama = $("#namaInput").val().trim();

	if(nama == ""){
		$("#errNama").text("Name tidak boleh kosong!");
		return;
	} else {
		$("#errNama").text("");
	}

	addMedicalFacilityCategoryName(function(medicalfacilitycategoryname) {
	    var isNameExist = medicalfacilitycategoryname.some(function(item) {
	        return item.name.toLowerCase() === nama.toLowerCase();
        });

        if (isNameExist) {
            $("#errNama").text("Nama sudah ada!");
            return;
        } else {
            $("#errNama").text("");
        }

	    var obj = {};
	    obj.name = nama;

	    var myJson = JSON.stringify(obj);

	    $.ajax({
		    url : "/api/addmedicalfacilitycategory",
		    type : "POST",
		    contentType : "application/json",
		    data : myJson,
		    success: function(data){
				$(".modal").modal("hide")
				location.reload();
		    },
		    error: function(){
			    alert("Terjadi kesalahan")
		    }
		});
	});
})

function addMedicalFacilityCategoryName(callback) {
    $.ajax({
        url: "/api/getallmedicalfacilitycategory",
        type: "GET",
        contentType: "application/json",
        success: function(medicalfacilitycategoryname) {
            if (callback && typeof callback === "function") {
                callback(medicalfacilitycategoryname);
            }
        },
        error: function(error) {
            if (callback && typeof callback === "function") {
                callback(error);
            }
        }
    });
}