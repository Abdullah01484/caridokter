$(document).ready(function() {
	GetMedicalFacilityCategoryById();

})

var createdOn;
var createdBy;

function GetMedicalFacilityCategoryById() {
	var id = $("#deleteMedicalFacilityCategoryId").val();
	$.ajax({
		url: "/api/getbyidmedicalfacilitycategory/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
		    createdBy = data.created_By;
            createdOn = data.created_On;
			$("#idDelete").text(data.name);
		}
	})
}

$("#deleteBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#deleteBtnDelete").click(function() {
	var id = $("#deleteMedicalFacilityCategoryId").val();
	$.ajax({
		url : "/api/deletemedicalfacilitycategory/" + id,
		type : "DELETE",
		contentType : "application/json",
		success: function(data){
            $(".modal").modal("hide")
            location.reload();
		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})