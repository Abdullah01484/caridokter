function getAllMedicalFacilityCategory(){
	$("#medicalfacilitycategoryTable").html(
		`<thead>
			<tr>
				<th width = '60%'>NAMA</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody id="medicalfacilitycategoryTBody"></tbody>
		`
	);

	$.ajax({
		url : "/api/getallmedicalfacilitycategory",
		type : "GET",
		contentType : "application/json",
		success: function(data){
			for(i = 0; i<data.length; i++){
				$("#medicalfacilitycategoryTBody").append(
					`
					<tr>
						<td>${data[i].name}</td>
                        <td>
                        	<button value="${data[i].id}" onClick="editMedicalFacilityCategory(this.value)" class="btn btn-warning">
                        	    <i class="bi-pencil-square"> Edit</i>
                        	</button>
                        	<button value="${data[i].id}" onClick="deleteMedicalFacilityCategory(this.value)" class="btn btn-danger">
                        	    <i class="bi-trash"> Delete</i>
                        	</button>
                        </td>
					</tr>
					`
				)
			}
		}
	});
}

$("#addBtn").click(function(){
	$.ajax({
		url: "/medicalfacilitycategory/addmedicalfacilitycategory",
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Tambah Kategori Produk Kesehatan");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
})

function editMedicalFacilityCategory(id){
	$.ajax({
		url: "/medicalfacilitycategory/editmedicalfacilitycategory/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Edit Kategori Produk Kesehatan");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

function deleteMedicalFacilityCategory(id){
	$.ajax({
		url: "/medicalfacilitycategory/deletemedicalfacilitycategory/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Delete Kategori Produk Kesehatan");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

function MedicalFacilityCategoryList(currentPage, length) {
    $.ajax({
		url : '/api/mappedmedicalfacilitycategory?page=' + currentPage + '&size=' + length,
		type : 'GET',
		contentType : 'application/json',
		success : function(data) {

			let table = "<table class='table table-bordered mt-3'>"

			table += "<tr> <th width='60%' >NAMA</th> <th>Action</th> </tr>"
			for (let i = 0; i < data.medicalfacilitycategory.length; i++) {
				table += "<tr>";
				table += "<td>" + data.medicalfacilitycategory[i].name + "</td>";
				table += "<td><button class='btn btn-primary btn-sm' value='" + data.medicalfacilitycategory[i].id + "' onclick=editMedicalFacilityCategory(this.value)><i class='bi-pencil-square'>  Edit</i></button> <button class='btn btn-danger btn-sm' value='" + data.medicalfacilitycategory[i].id + "' onclick=deleteMedicalFacilityCategory(this.value)><i class='bi-trash'></i> Delete</button></td>";
				table += "</tr>";
			}
				table += "</table>";
				table += "<br>"
				table += '<nav aria-label="Page navigation">';
				table += '<ul class="pagination">'
				table += '<li class="page-item"><a class="page-link" onclick="MedicalFacilityCategoryList(' + (data.currentPage - 1) + ',' + length + ')">Previous</a></li>'
				let index = 1;
				for (let i = 0; i < data.totalPages; i++) {
					table += '<li class="page-item"><a id="pageslink" class="page-link" onclick="MedicalFacilityCategoryList(' + i + ',' + length + ')">' + index + '</a></li>'
					index++;
				}
			table += '<li class="page-item"><a class="page-link" onclick="MedicalFacilityCategoryList(' + (data.currentPage + 1) + ',' + length + ')">Next</a></li>'
			table += '</ul>'
			table += '</nav>';
			$('#medicalfacilitycategoryList').html(table);
		}

	});
}

function SearchMedicalFacilityCategory(request) {
//console.log(request)

    if (request.length > 0)
	{
	    $.ajax({
			url: '/api/searchmedicalfacilitycategory/' + request,
			type: 'GET',
			contentType: 'application/json',
			success: function (result) {
			    let table = "<table class='table table-bordered mt-3'>";
				table += "<tr> <th width='60%' >NAMA</th> <th>Action</th> </tr>"
			    if (result.length > 0)
			    {
				    for (let i = 0; i < result.length; i++) {
					    table += "<tr>";
                        table += "<td>" + result[i].name + "</td>";
                        table += "<td><button class='btn btn-primary btn-sm' value='" + result[i].id + "' onclick=editMedicalFacilityCategory(this.value)><i class='bi-pencil-square'>  Edit</i></button> <button class='btn btn-danger btn-sm' value='" +  result[i].id + "' onclick=deleteMedicalFacilityCategory(this.value)><i class='bi-trash'></i> Delete</button></td>";
						table += "</tr>";
					}
				} else {
				    table += "<tr>";
				    table += "<td colspan='2' class='text-center'>No data</td>";
				    table += "</tr>";
		        }
				table += "</table>";
				$('#medicalfacilitycategoryList').html(table);
			}
		});
	} else {
        MedicalFacilityCategoryList(0,5);
	}
}

$(document).ready(function(){
    MedicalFacilityCategoryList(0,5);
//    getAllMedicalFacilityCategory();
})