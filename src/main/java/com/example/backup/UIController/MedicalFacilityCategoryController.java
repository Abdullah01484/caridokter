package com.example.backup.UIController;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("medicalfacilitycategory")
public class MedicalFacilityCategoryController {

    @RequestMapping("")
    public String medicalfacilitycategory(){ return ("medicalfacilitycategory/medicalfacilitycategory"); }

    @RequestMapping("addmedicalfacilitycategory")
    public String addmedicalfacilitycategory(){ return "medicalfacilitycategory/addMedicalFacilityCategory";}

    @RequestMapping("editmedicalfacilitycategory/{id}")
    public String editMedicalFacilityCategory(@PathVariable("id") Long id, Model model) {
        model.addAttribute("id", id);
        return "medicalfacilitycategory/editmedicalfacilitycategory";
    }
    @RequestMapping("deletemedicalfacilitycategory/{id}")
    public String deleteMedicalFacilityCategory(@PathVariable("id") Long id, Model model) {
        model.addAttribute("id", id);
        return "medicalfacilitycategory/deletemedicalfacilitycategory";
    }
}
