package com.example.backup.UIController;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("caridokter")
public class CariDokterController {

    @RequestMapping("")
    public String caridokter()
    {
        return ("caridokter/caridokter");
    }

    @RequestMapping("dashboardcaridokter")
    public String dashboardCariDokter()
    {
        return ("caridokter/dashboardcaridokter");
    }
}
