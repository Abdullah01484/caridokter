package com.example.backup.controllers;

import com.example.backup.models.MedicalFacilityCategory;
import com.example.backup.models.SpesialisasiDokter;
import com.example.backup.repositories.MedicalFacilityCategoryRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiMedicalFacilityCategoryController {
    @Autowired
    private MedicalFacilityCategoryRepo medicalFacilityCategoryRepo;

    @GetMapping("/getallmedicalfacilitycategory")
    public ResponseEntity<List<MedicalFacilityCategory>> GetAllMedicalFacilityCategory()
    {
        try {
            List<MedicalFacilityCategory> medicalfacilitycategory = this.medicalFacilityCategoryRepo.FindAllMedicalFacilityCategoryOrderByASC();
            return new ResponseEntity<>(medicalfacilitycategory, HttpStatus.OK);
        }

        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/getbyidmedicalfacilitycategory/{id}")
    public ResponseEntity<List<MedicalFacilityCategory>> GetMedicalFacilityCategoryById(@PathVariable("id") Long id)
    {
        try {
            Optional<MedicalFacilityCategory> medicalfacilitycategory = this.medicalFacilityCategoryRepo.findById(id);

            if (medicalfacilitycategory.isPresent())
            {
                ResponseEntity rest = new ResponseEntity<>(medicalfacilitycategory, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        }

        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/addmedicalfacilitycategory")
    public ResponseEntity<Object> SaveMedicalFacilityCategory(@RequestBody MedicalFacilityCategory medicalfacilitycategory)
    {
        try {
            medicalfacilitycategory.setCreated_By(1L);
            medicalfacilitycategory.setCreated_On(new Date());
            this.medicalFacilityCategoryRepo.save(medicalfacilitycategory);
            return new ResponseEntity<>(medicalfacilitycategory, HttpStatus.OK);
        }

        catch (Exception exception)
        {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("editmedicalfacilitycategory/{id}")
    public ResponseEntity<Object> EditMedicalFacilityCategory(@RequestBody MedicalFacilityCategory medicalfacilitycategory, @PathVariable("id") Long id)
    {
        Optional<MedicalFacilityCategory> medicalfacilitycategoryData = this.medicalFacilityCategoryRepo.findById(id);

        if (medicalfacilitycategoryData.isPresent())
        {
            medicalfacilitycategory.setId(id);
            medicalfacilitycategory.setModified_By(1L);
            medicalfacilitycategory.setModified_On(new Date());
            this.medicalFacilityCategoryRepo.save(medicalfacilitycategory);
            ResponseEntity rest =new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        }
        else
        {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("deletemedicalfacilitycategory/{id}")
    public String Deletemedicalfacilitycategory(@PathVariable("id") Long id)
    {
        try {
            MedicalFacilityCategory medicalfacilitycategory = this.medicalFacilityCategoryRepo.findByIdData(id);
            medicalfacilitycategory.setDeleted_By(1L);
            medicalfacilitycategory.setDeleted_On(new Date());
            medicalfacilitycategory.setIs_Delete(true);
            this.medicalFacilityCategoryRepo.save(medicalfacilitycategory);
            return "ok";
        }
        catch (Exception exception)
        {
            return "Data Not Found";
        }
    }

    @GetMapping("/searchmedicalfacilitycategory/{keyword}")
    public ResponseEntity<List<MedicalFacilityCategory>> SearchMedicalFacilityCategory(@PathVariable("keyword") String keyword)
    {
        if (keyword != null)
        {
            List<MedicalFacilityCategory> medicalfacilitycategory = this.medicalFacilityCategoryRepo.SearchMedicalFacilityCategory(keyword);
            return new ResponseEntity<>(medicalfacilitycategory, HttpStatus.OK);
        } else {
            List<MedicalFacilityCategory> spesialisasidokter = this.medicalFacilityCategoryRepo.findAll();
            return new ResponseEntity<>(spesialisasidokter, HttpStatus.OK);
        }
    }

    @GetMapping("/mappedmedicalfacilitycategory")
    public ResponseEntity<Map<String, Object>> GetAllPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size)
    {
        try {
            List<MedicalFacilityCategory> medicalfacilitycategory = new ArrayList<>();
            Pageable pagingSort = PageRequest.of(page, size);

            Page<MedicalFacilityCategory> pageTuts;

            pageTuts = medicalFacilityCategoryRepo.FindAllMedicalFacilityCategoryOrderByASC(pagingSort);

            medicalfacilitycategory = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("medicalfacilitycategory", medicalfacilitycategory);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
