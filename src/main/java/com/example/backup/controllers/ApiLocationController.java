package com.example.backup.controllers;

import com.example.backup.models.DokterTreatment;
import com.example.backup.models.Location;
import com.example.backup.repositories.LocationRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiLocationController {

    @Autowired
    private LocationRepo locationRepo;

    @GetMapping("/getalllocation")
    public ResponseEntity<List<Location>> GetAllLocation()
    {
        try {
//            List<Location> location = this.locationRepo.FindAllLocationOrderByASC();
            List<Location> location = this.locationRepo.findAll();
            return new ResponseEntity<>(location, HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
    @GetMapping("/getbyidlocation/{id}")
    public ResponseEntity<List<Location>>GetLocationById(@PathVariable("id") Long id)
    {
        try {
            Optional<Location> location = this.locationRepo.findById(id);

            if(location.isPresent()){
                ResponseEntity rest = new ResponseEntity<>(location, HttpStatus.OK);
                return rest;
            }else {
                return ResponseEntity.notFound().build();
            }
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
//    @GetMapping("/searchLocation/{keyword}")
//    public ResponseEntity<List<Location>> SearchKotaName(@PathVariable("keyword") String keyword)
//    {
//        if (keyword != null)
//        {
//            List<Location> location = this.locationRepo.SearchLocation(keyword);
//            return new ResponseEntity<>(location, HttpStatus.OK);
//        } else {
//            List<Location> location = this.locationRepo.findAll();
//            return new ResponseEntity<>(location, HttpStatus.OK);
//        }
//    }
//    @GetMapping("/locationMapped")
//    public ResponseEntity<Map<String, Object>> GetAllPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size)
//    {
//        try {
//            List<Location> location = new ArrayList<>();
//            Pageable pagingSort = PageRequest.of(page, size);
//
//            Page<Location> pageTuts;
//
//            pageTuts = locationRepo.FindAllLocationOrderByASC(pagingSort);
//
//            location = pageTuts.getContent();
//
//            Map<String, Object> response = new HashMap<>();
//            response.put("location", location);
//            response.put("currentPage", pageTuts.getNumber());
//            response.put("totalItems", pageTuts.getTotalElements());
//            response.put("totalPages", pageTuts.getTotalPages());
//
//            return new ResponseEntity<>(response, HttpStatus.OK);
//        }
//
//        catch (Exception e)
//        {
//            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//    }
    @PostMapping("/addlocation")
    public ResponseEntity<Object> SaveLocation(@RequestBody Location location)
    {
        try {
            location.setCreated_By(1L);
            location.setCreated_On(new Date());
            this.locationRepo.save(location);
            return new ResponseEntity<>("Success", HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }
    @PutMapping("/editlocation/{id}")
    public ResponseEntity<Object> UpdateLocation(@RequestBody Location location, @PathVariable("id") Long id)
    {
        Optional<Location> locationData = this.locationRepo.findById(id);

        if (locationData.isPresent()){
            location.setModified_By(1L);
            location.setModified_On(new Date());
            location.setId(id);
            this.locationRepo.save(location);
            ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        }else {
            return ResponseEntity.notFound().build();
        }
    }
    @DeleteMapping("/deletelocation/{id}")
    public String DeleteLocation(@PathVariable("id") Long id)
    {
        try {
            Location location = this.locationRepo.findByIdData(id);
            location.setDeleted_By(1L);
            location.setDeleted_On(new Date());
            location.setIs_Delete(true);
            this.locationRepo.save(location);
            return "ok";
        }
        catch (Exception exception)
        {
            return "Data Not Found";
        }
    }
}
