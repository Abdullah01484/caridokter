package com.example.backup.controllers;

import com.example.backup.models.LocationLevel;
import com.example.backup.repositories.LocationLevelRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiLocationLevelController {

    @Autowired
    private LocationLevelRepo locationLevelRepo;

    @GetMapping("/getalllocationlevel")
    public ResponseEntity<List<LocationLevel>> GetAllLocationLevel()
    {
        try {
            List<LocationLevel> category = this.locationLevelRepo.findAllNotDeleted();
            return new ResponseEntity<>(category, HttpStatus.OK);
        }

        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/addlocationlevel")
    public ResponseEntity<Object> SaveLocationLevel(@RequestBody LocationLevel locationLevel)
    {
        try {
            locationLevel.setCreated_By(1L);
            locationLevel.setCreated_On(new Date());
            this.locationLevelRepo.save(locationLevel);
            return new ResponseEntity<>(locationLevel, HttpStatus.OK);
        }

        catch (Exception exception)
        {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getbyidlocationlevel/{id}")
    public ResponseEntity<List<LocationLevel>> GetLocationLevelById(@PathVariable("id") Long id)
    {
        try {
            Optional<LocationLevel> locationLevel = this.locationLevelRepo.findById(id);

            if (locationLevel.isPresent())
            {
                ResponseEntity rest = new ResponseEntity<>(locationLevel, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        }

        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("/editlocationlevel/{id}")
    public ResponseEntity<Object> EditLocationLevel(@RequestBody LocationLevel locationLevel, @PathVariable("id") Long id)
    {
        Optional<LocationLevel> locationLevelData = this.locationLevelRepo.findById(id);

        if (locationLevelData.isPresent())
        {
            locationLevel.setModified_By(1L);
            locationLevel.setModified_On(new Date());
            locationLevel.setId(id);
            this.locationLevelRepo.save(locationLevel);
            ResponseEntity rest =new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        }
        else
        {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/deletelocationlevel/{id}")
    public String DeleteLocationLevel(@PathVariable("id") Long id)
    {
        try {
            LocationLevel locationLevel = this.locationLevelRepo.findByIdData(id);
            locationLevel.setDeleted_By(1L);
            locationLevel.setDeleted_On(new Date());
            locationLevel.setIs_Delete(true);
            this.locationLevelRepo.save(locationLevel);
            return "ok";
        }
        catch (Exception exception)
        {
            return "Data Not Found";
        }
    }
    @GetMapping("locationlevelmapped")
    public ResponseEntity<Map<String, Object>> GetAllPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5")int size)
    {
        try{
            List <LocationLevel> locationlevel = this.locationLevelRepo.findAllNotDeleted();

            int start = page * size;
            int end = Math.min((start + size), locationlevel.size());

            List<LocationLevel> paginatedLocationLevel = locationlevel.subList(start, end);

            Map<String, Object> response = new HashMap<>();
            response.put("locationlevel", paginatedLocationLevel);
            response.put("currentPage", page);
            response.put("totalItems", locationlevel.size());
            response.put("totalPages",(int)Math.ceil((double) locationlevel.size() / size));

            return new ResponseEntity<>(response,HttpStatus.OK);
        }

        catch (Exception exception){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @GetMapping("/searchlocationlevel/{keyword}")
    public ResponseEntity<List<LocationLevel>> SearchLocationLevelName(@PathVariable("keyword") String keyword)
    {
        if (keyword != null)
        {
            List<LocationLevel> locationLevel = this.locationLevelRepo.SearchLocationLevel(keyword);
            return new ResponseEntity<>(locationLevel, HttpStatus.OK);
        } else {
            List<LocationLevel> locationLevel = this.locationLevelRepo.findAllNotDeleted();
            return new ResponseEntity<>(locationLevel, HttpStatus.OK);
        }
    }
}


