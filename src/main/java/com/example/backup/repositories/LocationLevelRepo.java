package com.example.backup.repositories;

import com.example.backup.models.LocationLevel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface LocationLevelRepo extends JpaRepository<LocationLevel, Long> {
    @Query(value = "SELECT * FROM m_location_level WHERE id = :id", nativeQuery = true)
    LocationLevel findByIdData(long id);
    @Query(value = "SELECT * FROM m_location_level WHERE is_delete = false ORDER BY name ASC" , nativeQuery = true)
    List<LocationLevel> findAllNotDeleted();
    @Query(value = "FROM LocationLevel WHERE lower(name) LIKE lower(concat('%',?1,'%')) AND is_Delete = false")
    List<LocationLevel> SearchLocationLevel(String keyword);
    @Query(value = "SELECT * FROM m_location_level ORDER BY name ASC", nativeQuery = true)
    List<LocationLevel> FindAllLocationLevelOrderByASC();

    @Query(value = "SELECT * FROM m_location_level ORDER BY name ASC", nativeQuery = true)
    Page<LocationLevel> FindAllLocationLevelOrderByASC(Pageable pageable);
}
