package com.example.backup.repositories;

import com.example.backup.models.DokterTreatment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface DokterTreatmentRepo extends JpaRepository<DokterTreatment, Long> {
    @Query(value = "SELECT * FROM t_doctor_treatment WHERE id = :id", nativeQuery = true)
    DokterTreatment findByIdData (long id);

    @Query(value = "SELECT * FROM t_doctor_treatment WHERE id= :id", nativeQuery = true)
    List<DokterTreatment> findByIdProfil(Long id);

    @Query(value = "SELECT * FROM t_doctor_treatment WHERE t_doctor_treatment.doctor_id = :id", nativeQuery = true)
    List<DokterTreatment> FindBYDoctorID(Long id);
}
