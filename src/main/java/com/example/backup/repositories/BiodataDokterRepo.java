package com.example.backup.repositories;

import com.example.backup.models.BiodataDokter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface BiodataDokterRepo extends JpaRepository<BiodataDokter, Long> {
    @Query(value = "SELECT * FROM m_doctor WHERE id = :id", nativeQuery = true)
    BiodataDokter findByIdData(long id);
}
