package com.example.backup.repositories;

import com.example.backup.models.MedicalFacilityCategory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MedicalFacilityCategoryRepo extends JpaRepository<MedicalFacilityCategory, Long> {

    @Query(value = "SELECT * FROM m_medical_facility_category WHERE is_delete = false ORDER BY name ASC", nativeQuery = true)
    List<MedicalFacilityCategory> FindAllMedicalFacilityCategoryOrderByASC();

    @Query(value = "SELECT * FROM m_medical_facility_category WHERE id = :id", nativeQuery = true)
    MedicalFacilityCategory findByIdData(long id);

    //    pencarian
    @Query("FROM MedicalFacilityCategory WHERE is_delete = false AND lower(name) LIKE lower(concat('%',?1,'%') ) ")
    List<MedicalFacilityCategory> SearchMedicalFacilityCategory(String keyword);

    @Query(value = "SELECT * FROM m_medical_facility_category WHERE is_delete = false ORDER BY name ASC", nativeQuery = true)
    Page<MedicalFacilityCategory> FindAllMedicalFacilityCategoryOrderByASC(Pageable pageable);
}
