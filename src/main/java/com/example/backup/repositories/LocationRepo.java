package com.example.backup.repositories;

import com.example.backup.models.Biodata;
import com.example.backup.models.Location;
import com.example.backup.models.SpesialisasiDokter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface LocationRepo extends JpaRepository<Location, Long> {
//    @Query("FROM m_location WHERE lower(name) LIKE lower(concat('%',?1,'%') ) ")
//    List<Location> SearchLocation(String keyword);
//    @Query(value = "SELECT * FROM m_location ORDER BY name ASC", nativeQuery = true)
//    Page<Location> FindAllLocationOrderByASC(Pageable pageable);
    @Query(value = "SELECT * FROM m_location ORDER BY name ASC", nativeQuery = true)
    List<Location> FindAllLocationOrderByASC();

    @Query(value = "SELECT * FROM m_location WHERE id = :id", nativeQuery = true)
    Location findByIdData(long id);
}
