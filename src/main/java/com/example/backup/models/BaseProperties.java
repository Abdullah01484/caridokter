package com.example.backup.models;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@MappedSuperclass
public class BaseProperties {

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_on", nullable = false)
    private Date created_On;

    @Column(name = "created_by", nullable = false)
    private long created_By;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "modified_on", nullable = true)
    private Date modified_On;

    @Column(name = "modified_by", nullable = true)
    private long modified_By;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "deleted_on", nullable = true)
    private Date deleted_On;

    @Column(name = "deleted_by", nullable = true)
    private Long deleted_By;

    @Column(name = "is_delete", nullable = false)
    private Boolean is_Delete = false;

    public Date getCreated_On() {
        return created_On;
    }

    public void setCreated_On(Date created_On) {
        this.created_On = created_On;
    }

    public long getCreated_By() {
        return created_By;
    }

    public void setCreated_By(long created_By) {
        this.created_By = created_By;
    }

    public Date getModified_On() {
        return modified_On;
    }

    public void setModified_On(Date modified_On) {
        this.modified_On = modified_On;
    }

    public long getModified_By() {
        return modified_By;
    }

    public void setModified_By(long modified_By) {
        this.modified_By = modified_By;
    }

    public Date getDeleted_On() {
        return deleted_On;
    }

    public void setDeleted_On(Date deleted_On) {
        this.deleted_On = deleted_On;
    }

    public Long getDeleted_By() {
        return deleted_By;
    }

    public void setDeleted_By(Long deleted_By) {
        this.deleted_By = deleted_By;
    }

    public Boolean getIs_Delete() {
        return is_Delete;
    }

    public void setIs_Delete(Boolean is_Delete) {
        this.is_Delete = is_Delete;
    }
}
