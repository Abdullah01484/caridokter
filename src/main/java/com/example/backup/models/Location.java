package com.example.backup.models;

import javax.persistence.*;

@Entity
@Table(name = "m_location")
public class Location extends BaseProperties{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", length = 100)
    private String name;

    @ManyToOne
    @JoinColumn(name = "parent_id", insertable = false, updatable = false)
    private Location parentLocation;

    @Column(name = "parent_id", nullable = true)
    private Long parent_id;

    @ManyToOne
    @JoinColumn(name = "location_level_id", insertable = false, updatable = false)
    private LocationLevel locationLevel;

    @Column(name = "location_level_id", nullable = true)
    private Long lokasi;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getParentLocation() {
        return parentLocation;
    }

    public void setParentLocation(Location parentLocation) {
        this.parentLocation = parentLocation;
    }

    public Long getParent_id() {
        return parent_id;
    }

    public void setParent_id(Long parent_id) {
        this.parent_id = parent_id;
    }

    public LocationLevel getLocationLevel() {
        return locationLevel;
    }

    public void setLocationLevel(LocationLevel locationLevel) {
        this.locationLevel = locationLevel;
    }

    public Long getLokasi() {
        return lokasi;
    }

    public void setLokasi(Long lokasi) {
        this.lokasi = lokasi;
    }
}
