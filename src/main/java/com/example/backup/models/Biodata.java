package com.example.backup.models;

import javax.persistence.*;
import java.sql.Blob;

@Entity
@Table(name = "m_biodata")
public class Biodata extends BaseProperties{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private long id;

    @Column(name = "fullname", length = 255)
    private String fullname;

    @Column(name = "mobile_phone", length = 15)
    private String mobile_phone;

    @Column(name = "image")
    private Blob image;

    @Column(name = "image_path", length = 225)
    private String image_path;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getMobile_phone() {
        return mobile_phone;
    }

    public void setMobile_phone(String mobile_phone) {
        this.mobile_phone = mobile_phone;
    }

    public Blob getImage() {
        return image;
    }

    public void setImage(Blob image) {
        this.image = image;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }
}
