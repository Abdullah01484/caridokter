package com.example.backup.models;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "t_doctor_office")
public class DoctorOffice extends BaseProperties{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private long id;

    @ManyToOne
    @JoinColumn(name = "doctor_id", insertable = false, updatable = false)
    public BiodataDokter biodataDokter;

    @Column(name = "doctor_id")
    private long doctor_id;

    @ManyToOne
    @JoinColumn(name = "medical_facility_id", insertable = false, updatable = false)
    public MedicalFacility medicalFacility;

    @Column(name = "medical_facility_id")
    private long medical_facility_id;

    @ManyToOne
    @JoinColumn(name = "specialization_id", insertable = false, updatable = false)
    public SpesialisasiDokter specialization;

    @Column(name = "specialization_id")
    private Long specialization_id;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy", timezone = "Asia/jakarta")
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "start_date", nullable = false)
    private Date start_date;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy", timezone = "Asia/jakarta")
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "end_date")
    private Date end_date;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public BiodataDokter getBiodataDokter() {
        return biodataDokter;
    }

    public void setBiodataDokter(BiodataDokter biodataDokter) {
        this.biodataDokter = biodataDokter;
    }

    public long getDoctor_id() {
        return doctor_id;
    }

    public void setDoctor_id(long doctor_id) {
        this.doctor_id = doctor_id;
    }

    public MedicalFacility getMedicalFacility() {
        return medicalFacility;
    }

    public void setMedicalFacility(MedicalFacility medicalFacility) {
        this.medicalFacility = medicalFacility;
    }

    public long getMedical_facility_id() {
        return medical_facility_id;
    }

    public void setMedical_facility_id(long medical_facility_id) {
        this.medical_facility_id = medical_facility_id;
    }

    public SpesialisasiDokter getSpecialization() {
        return specialization;
    }

    public void setSpecialization(SpesialisasiDokter specialization) {
        this.specialization = specialization;
    }

    public Long getSpecialization_id() {
        return specialization_id;
    }

    public void setSpecialization_id(Long specialization_id) {
        this.specialization_id = specialization_id;
    }

    public Date getStart_date() {
        return start_date;
    }

    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    public Date getEnd_date() {
        return end_date;
    }

    public void setEnd_date(Date end_date) {
        this.end_date = end_date;
    }
}
